<?php

declare(strict_types=1);

namespace Adrian;

use Exception;
use League\OpenAPIValidation\PSR7\Exception\Validation\AddressValidationFailed;
use League\OpenAPIValidation\PSR7\Exception\Validation\InvalidParameter;
use League\OpenAPIValidation\PSR7\Exception\ValidationFailed;
use League\OpenAPIValidation\PSR7\OperationAddress;
use League\OpenAPIValidation\PSR7\ValidatorBuilder;
use League\OpenAPIValidation\Schema\Exception\KeywordMismatch;
use Nyholm\Psr7\Factory\Psr17Factory;
use Symfony\Bridge\PsrHttpMessage\Factory\PsrHttpFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SchemaValidator
{
    /**
     * @var array
     */
    protected static $errors;

    /**
     * @var PsrHttpFactory
     */
    protected $psrHttpFactory;

    /**
     * @var ValidatorBuilder
     */
    protected $validator;

    public function __construct(string $specPath)
    {
        $psr17Factory         = new Psr17Factory();
        $this->psrHttpFactory = new PsrHttpFactory($psr17Factory, $psr17Factory, $psr17Factory, $psr17Factory);
        $this->validator      = (new ValidatorBuilder())->fromYamlFile($specPath);
        self::$errors         = [];
    }

    /**
     * @param Request $request
     *
     * @throws
     */
    public function validateRequest(Request $request): void
    {
        $psr7request = $this->psrHttpFactory->createRequest($request);

        $validator = $this->validator->getServerRequestValidator();

        $validator->validate($psr7request);
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function isValidRequest(Request $request): bool
    {
        try {
            $this->validateRequest($request);

            return true;
        } catch (ValidationFailed $e) {
            $this->addErrors($e);

            return false;
        }
    }

    /**
     * @param Response $response
     * @param string   $schemaUri
     * @param string   $method
     *
     * @throws
     */
    public function validateResponse(Response $response, string $schemaUri, string $method): void
    {
        $operation = new OperationAddress($schemaUri, $method);

        $psr7Response = $this->psrHttpFactory->createResponse($response);

        $validator = $this->validator->getResponseValidator();

        $validator->validate($operation, $psr7Response);
    }

    /**
     * @param Response $response
     * @param string   $schemaUri
     * @param string   $method
     *
     * @return bool
     */
    public function isValidResponse(Response $response, string $schemaUri, string $method): bool
    {
        try {
            $this->validateResponse($response, $schemaUri, $method);

            return true;
        } catch (ValidationFailed $e) {
            $this->addErrors($e);

            return false;
        }
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return static::$errors;
    }

    /**
     * @param Exception $exception
     */
    public function addErrors(Exception $exception): void
    {
        $prev = $exception->getPrevious();

        if ($prev === null) {
            $key = 'undefined';
            if ($exception instanceof AddressValidationFailed) {
                $key = $exception->getAddress()->path();
            }
            $message = $exception->getMessage();
        } else {
            $key = 'undefined';
            if ($prev instanceof KeywordMismatch) {
                $keywords = $prev->dataBreadCrumb()->buildChain();
                $key      = end($keywords);
            } elseif ($prev instanceof InvalidParameter) {
                $key = $prev->name();
            }

            $message = $prev->getMessage();
        }

        static::$errors[$key] = $message;
    }
}
