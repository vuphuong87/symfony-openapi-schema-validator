<?php

declare(strict_types=1);

namespace Adrian\Tests;

use Adrian\SchemaValidator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class SchemaValidatorTest extends TestCase
{
    /** @var SchemaValidator */
    protected $validator;

    public function setUp()
    {
        parent::setUp();

        $this->validator = new SchemaValidator(__DIR__ . '/Fixtures/sample.yaml');
    }

    /**
     * @dataProvider dpForTestValidateRequest
     *
     * @param string     $uri
     * @param string     $method
     * @param array|null $parameters
     * @param bool       $expectedResult
     * @param array      $expectedErrorKeys
     */
    public function testValidateRequest(
        string $uri,
        string $method,
        ?array $parameters,
        bool $expectedResult,
        array $expectedErrorKeys
    ) {
        $request = Request::create(
            $uri,
            $method,
            $parameters ?: [],
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $parameters ? json_encode($parameters) : null
        );

        $result = $this->validator->isValidRequest($request);
        self::assertSame($expectedResult, $result);
        self::assertSame(array_keys($this->validator->getErrors()), $expectedErrorKeys);
    }

    public function dpForTestValidateRequest()
    {
        return [
            'valid query param'   => [
                '/v1/posts',
                'GET',
                [
                    'page_size' => 50,
                ],
                true,
                [],
            ],
            'valid body param'    => [
                '/v1/posts',
                'POST',
                [
                    'title'   => 'Title 1',
                    'content' => 'Content 1',
                ],
                true,
                [],
            ],
            'invalid query param' => [
                '/v1/posts',
                'GET',
                [
                    'page' => 0,
                ],
                false,
                ['page'],
            ],
            'invalid body param'  => [
                '/v1/posts',
                'POST',
                [
                    'title' => 'Title 1',
                ],
                false,
                ['content'],
            ],
            'empty body param'    => [
                '/v1/posts',
                'POST',
                null,
                false,
                ['/v1/posts'],
            ],
            // @TODO validator need to cover this case
            /*'disallow additional field' => [
                '/v1/posts',
                'POST',
                [
                    'id'      => '37dbc026-70bd-41ed-a410-8c980a6e632f',
                    'title'   => 'Post 1',
                    'content' => 'Content 1'
                ],
                false,
                ['undefined'],
            ],*/
        ];
    }

    /**
     * @dataProvider dpForTestValidateResponse
     *
     * @param string $uri
     * @param string $method
     * @param array  $response
     * @param bool   $expectedResult
     * @param array  $expectedErrorKeys
     */
    public function testValidateResponse(
        string $uri,
        string $method,
        array $response,
        bool $expectedResult,
        array $expectedErrorKeys
    ) {
        $response = new JsonResponse($response);

        $result = $this->validator->isValidResponse($response, $uri, $method);
        self::assertSame($expectedResult, $result);
        self::assertSame(array_keys($this->validator->getErrors()), $expectedErrorKeys);
    }

    public function dpForTestValidateResponse()
    {
        return [
            'valid response'         => [
                '/v1/posts',
                'get',
                [
                    [
                        'id'      => '37dbc026-70bd-41ed-a410-8c980a6e632f',
                        'title'   => 'Post 1',
                        'content' => 'Content 1',
                    ],
                ],
                true,
                [],
            ],
            'lack of `content`'      => [
                '/v1/posts',
                'get',
                [
                    [
                        'id'    => '37dbc026-70bd-41ed-a410-8c980a6e632f',
                        'title' => 'Post 1',
                    ],
                ],
                false,
                ['content'],
            ],
            'invalid format of `id`' => [
                '/v1/posts',
                'get',
                [
                    [
                        'id'      => '123-123-123',
                        'title'   => 'Post 1',
                        'content' => 'Content 1',
                    ],
                ],
                false,
                ['id'],
            ],
            // @TODO validator need to cover this case
            /*'disallow additional field' => [
                '/v1/posts',
                'get',
                [
                    [
                        'id'      => '37dbc026-70bd-41ed-a410-8c980a6e632f',
                        'title'   => 'Post 1',
                        'content' => 'Content 1',
                        'author'  => 'Adrian',
                    ],
                ],
                false,
                ['undefined'],
            ],*/
        ];
    }
}