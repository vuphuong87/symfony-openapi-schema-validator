# Symfony OpenAPI Schema Validator

[![coverage report](https://gitlab.com/vuphuong87/symfony-openapi-schema-validator/badges/master/coverage.svg)](https://gitlab.com/vuphuong87/symfony-openapi-schema-validator/-/commits/master)

The OpenAPI schema validator wrapper for Symfony HTTP Request / Response object.

This package use `league/openapi-psr7-validator` package to validate OpenAPI schema on a PSR7 Request / Response object. 
The Symfony HTTP Request / Response object will be converted to PSR7 objects thanks to `symfony/psr-http-message-bridge` package.

## Install

``` bash
$ composer require vuphuong87/symfony-openapi-schema-validator
```

## Usage

### Validate a Request

``` php
$schemaValidator = new SchemaValidator($apispec);
$schemaValidator->isValidRequest($request)
```

### Validate a Response

``` php
$path = '/v1/posts'; // uri path defined in schema
$schemaValidator = new SchemaValidator($apispec);
$schemaValidator->isValidResponse($response, $path, 'GET');
```

### Get validation errors

``` php
$schemaValidator->getErrors();
```